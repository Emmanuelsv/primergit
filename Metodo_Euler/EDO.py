
import numpy as np


class solNumerica():
    def __init__(self,a,b,n,y0,f,point=False) :
        print("Inizializando soluciones numericas")
       
        self.a = a
        self.b = b
        self.n =n
        self.y0=y0
        self.f=f

        self.x = []
        self.y = []
        self.point = point

    def X(self):
        if self.point:
            self.b = self.point
            self.x = np.arange(self.a,self.point,self.h())

        else :
            self.x = np.arange(self.a,self.b,self.h())

    def h(self):
        return (self.b- self.a)/self.n
    
    def Euler(self):

        self.X()
        #self.x=np.arrange(self.a,self.b,self.h)
        self.y =[self.y0]

        for i in range(self.n):
            self.y.append(self.y[i] + self.h() * self.f(self.x[i],self.y[i]))

            return self.x, self.y
        
    #def Rk4(self):


     #   return self.x, self.y