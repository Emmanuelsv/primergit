import numpy as np 
import math
import matplotlib.pyplot as plt


class Eulerm():
    def __init__(self,x0,y0,xf):

        self.x0 =x0
        self.y0 =y0
        self.xf =xf

    def pX(self):
        X = np.arange(self.x0,self.xf,0.01) 
        return X
        
        
    def H(self):
        h =(self.xf-self.x0)/100
        return h
  

        #def val1(self):
         #   v1 = self.y0 + self.H() * (-2*self.x0 + self.y0 * self.x0)
          #  return v1
        
    def metodoE(self):
        y = np.zeros(100)
        x = self.pX()
        y [0] = -3
        #n = np.arange(10)
        for i in range(100):
            if i <= 98:
                y[i+1] = y[i] + self.H() * -2* x[i] *y[i]
         

        return y
            

    def graf(self):
        plt.figure(figsize=[10,8])
        plt.plot(self.pX(),self.metodoE())
        plt.savefig("Metodo_Euler.png")

    

        