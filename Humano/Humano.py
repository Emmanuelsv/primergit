




class humano():
    #Constructor
    def __init__(self,edad,nombre):

        #Atributos
        
        self.edad=edad
        self.nombre=nombre


    # Metodo
    def viewEdad(self):
        a=self.edad
        return a

    def viewName(self):
        return self.nombre

    def InfoHumano(self):
        return "{} tiene {} años".format(self.viewName(),self.viewEdad()) 


class humano2(humano):

    def __init__(self, edad, nombre,peso):
        super().__init__(edad, nombre)

        self.peso=peso

    def viewName(self):
        return super().viewName()