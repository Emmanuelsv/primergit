import math
import numpy as np
import matplotlib.pyplot as plt

class tiro_parabolico():
    
    def __init__(self,vo,xo,ho,radtheta,g):

        #Metodos
        self.vo = vo
        self.radtheta =math.radians(self.radtheta)
        self.xo=xo
        self.ho=ho
        self.g=g



    def voX(self):
        vox = self.vo * round(math.cos(self.radtheta),3)
        return  vox

    def voY(self):
        voy = self.vo * round(math.sin(self.radtheta),3)
        return  voy

    def tiempoV(self):
        try:
            tv = (-self.voY() + np.sqrt(self.voY()**2 - 2* self.g * self.ho))/self.g
            return tv 
        except:
            return "Error en el calculo de tiempo de vuelo, revisar parametros"
        
    def aTime(self):
        a_time = np.arange(0,self.tiempoV(),0.001)
        return a_time
            
    def posX(self):
        posx = [self.xo + self.voX() * i for i in self.aTime()]
        return  posx

    def posY(self):
        posy = [self.ho + self.voY() * i + (1/2)* self.g * i**2 for i in self.aTime]
        return  posy
    
    def figMp(self):
        plt.figure(figsize=[10,8])
        plt.plot(self.posX(),self.posY())
        plt.savefig("trayectoria.png")
